let t, k, ls;

const chooseOptimalDistance = (t, k, ls) => {

      /* Функція повертає "найкращу" суму, 
    тобто найбільшу можливу суму k відстаней, 
    менших або рівних заданій межі t, 
    якщо ця сума існує, або якщо не існує - None */

    let longestDistance = 0; // змінна для збереження найкращої суми, далі будем порівнювати

    for (let singleDistance of ls) {
        //для кожного елементу списка ls знаходимо найкращу суму
        if (k == 1 && longestDistance < singleDistance && singleDistance < t){
            longestDistance = singleDistance;
        }
        //у інших випадках рекурсивно викликаємо цю ж функцію

        else {
            // підготуємо аргументи:
            // межу зменшимо на поточний елемент списку ls 
            // cуму відстаней k зменшимо на 1 
            // з списку ls виключимо поточний елемент списку (single_distance)
            let newT = t - singleDistance;
            let newK = k - 1;
            let newLS = ls.slice();
            newLS.pop(singleDistance);
            console.log(ls.singleDistance)
            // отримаємо "найкращу" суму для шляху без поточного елементу

            let reccursionResult = chooseOptimalDistance (newT, newK, newLS);
            //якщо результат не None, додамо поточний елемент,
            //щоб отримати довжину усього шляху разом з поточним елементом
            //збережемо найбільшу таку суму порівнюючи з останньої збереженою
            if (reccursionResult && reccursionResult + singleDistance > longestDistance){
            longestDistance = reccursionResult + singleDistance;
            }
        }
    }
    //якщо знайшовся хоч один результат, що пройшов усі перевірки,
    // а бажано - найбільший, повертаємо його як результат функції
    if (longestDistance != 0)
    return longestDistance;
}

let result1 = chooseOptimalDistance(174, 3, [51, 56, 58, 59, 61]); 
console.log(result1)

let result2 = chooseOptimalDistance(163, 3, [50])
console.log(result2)
